﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using cs241Lab4Lib;

namespace cs241Lab4Main
{
    class Program
    {
        static void Main(string[] args)
        {

            //// Determine the time required to perform a simple sort.
            //int seed = 2344121;
            //StreamWriter sortWriter = new StreamWriter("simpleSort.txt");

            //Stopwatch timer = new Stopwatch();

            //// Determine time for multiple array lengths
            //for (int i = 10; i <= 10000; i *= 2)
            //{
            //    int[] numbers = new int[i];
            //    Analysis.MakeRandom(numbers, i, seed, true);

            //    timer.Start();
            //    Analysis.Sort(numbers);
            //    timer.Stop();
            //    sortWriter.WriteLine("{0} {1}", i, timer.ElapsedTicks);
            //    Console.WriteLine("{0} {1}", i, timer.ElapsedTicks);
            //    timer.Reset();
            //}
            //sortWriter.Close();// Create a file called simpleSort.txt

            // Repeat the procedure used above to obtain times for:
            //   The library sort Array.Sort(numbers)
            //   The ThreeSums program from Sedgewick
            //   The BinarySearch program 
            //      Modify the program on p381 to provide the index of 
            //      the searched for key, or -1 if the key does not exist.

            // Set the upper-limits so that they program takes a few minutes
            // to complete.  If a program takes longer than 10 minutes to 
            // complete, reset the upper-limit so the program completes in
            // less time.

            // Graph the results (number of elements by number of ticks)
            // You may use any program you like to generate the graphs.
            // Excel is probably a good choice.
            // Graph the data using linear and log-log plots.

        }
    }
}
